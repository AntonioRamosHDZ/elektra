const express = require('express');
const app = express();

//Configuración server
app.set('port', process.env.PORT || 3000);


//Middlewares
app.use(express.json());

//Rutas
app.use(require('./routes/electra'));


app.listen(app.get('port'), () => {
    console.log("Servidor escuchando en el puerto: " + app.get('port'));
})